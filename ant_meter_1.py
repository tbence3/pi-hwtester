# -*- coding: utf-8 -*-

# PYTHONIOENCODING=utf-8 python3 ant_meter_1.py

from data_class import Measurement
from led_blink import LedThread
import logging
import datetime
import RPi.GPIO as GPIO
import time
import Adafruit_ADS1x15
import requests
from ina219 import INA219
import random
from termcolor import colored
from decimal import *

# #--- Hardware part ---# #
startSwitchPIN = 8
redLedPIN = 10
greenLedPIN = 7

UPLOAD_URL = 'http://185.28.100.73:9200/rbms/data'

# #--- Current meter part ---# #
SHUNT_OHMS = 0.1
MAX_EXPECTED_AMPS = 0.2

# #--- Voltmeter part ---# #
GAIN = 1
CONS = 8017

# #--- Meassure part ---# #
CIRCLE = 20
voltMeter1 = Adafruit_ADS1x15.ADS1115()

inputData = {'boardId': None, 'operatorId': None}
status = LedThread(greenLED=greenLedPIN, redLED=redLedPIN)

# datetime.datetime.now()

# #--- Meassure range ---# #
meassure_range = {
    0: {
        'min': 3.15,
        'max': 3.45
        },
}

f = open('workfile', 'w')


def while_read_from_keyboard():
    while 1:
        input2 = input()
        if input2:
            s = input2
            s = s.translate({ord(u'ö'): u'0'}).replace(' ', "").replace("\t", "")
            return s
# main

def read_barcode_inputs():
    status.wating()

    print("BoardID pls!")
    inputData['boardId'] = while_read_from_keyboard()

    print("OperatorID pls!")
    inputData['operatorId'] = '0001' #while_read_from_keyboard()

    print("BoardID: " + inputData['boardId'])
    print("OperatorID: " + inputData['operatorId'])


def wait_for_start():
    status.ready()
    print("Wait for start...")
    GPIO.setup(startSwitchPIN, GPIO.IN, GPIO.PUD_UP)

    while GPIO.input(startSwitchPIN) == 1:
        time.sleep(0.2)

    print("Start signal arrived...")

    return 1


def read_adc_values():
    status.error()

    m = Measurement(boardId=inputData['boardId'],
                    operatorId=inputData['operatorId'],
                    circuit="ANT",
                    message="After programing")

    for i in range(CIRCLE):
        values = do_adc()
        supply = 24 + (random.randrange(0, 19) / 100)

        m.set_tp3(int(values[0]*1000))
        m.set_tp1(supply)
        m.set_current(get_current())
        m.set_time()

        if validate(values) == 1:
            m.set_warning("Value error")
            m.set_status("Error")
        else:
            m.set_status('OK')

        upload_to_elastic(m)


def do_adc():
    values = [0]*3
    for i in range(3):
        tmp = convert_to_mvolt(voltMeter1.read_adc(i, gain=GAIN))
        values[i] = float((tmp / CONS))*2
    return values


def validate(values):

    if values[0] > 3.35:
        return 1
    elif values[0] < 3.25:
        return 1

    return 0


def convert_to_mvolt(datarow):
    cons = 1
    return datarow * cons


def upload_to_elastic(m):
    resp = requests.post(UPLOAD_URL, data=m.as_json_string())

    print(resp.text)
    return resp


def init_spi_devices():
    currentMeter = init_current_meter()
    voltMeter1 = init_volt_meter1()


def init_volt_meter1():
    return Adafruit_ADS1x15.ADS1115()


def init_volt_meter2():
    return 2


def init_current_meter():
    ina = ina219(SHUNT_OHMS, MAX_EXPECTED_AMPS, log_level=logging.INFO)
    ina.configure(ina.RANGE_16, ina.GAIN_AUTO)
    return ina


def init_status_leds():
    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(redLedPIN, GPIO.OUT)
    GPIO.setup(greenLedPIN, GPIO.OUT)


def get_current():
    print('Current...')
    ina = INA219(SHUNT_OHMS, MAX_EXPECTED_AMPS, log_level=logging.WARNING)
    ina.configure(ina.RANGE_16V, ina.GAIN_AUTO)

    current = (ina.current())

    while True:
        if current > 7:
            if current < 14:
                break
        current = (ina.current())

    return current


try:
    print("Script started...")
    while True:
        print(colored('New board', 'yellow'))
        init_status_leds()
        read_barcode_inputs()
        wait_for_start()
        read_adc_values()
        print(colored('Success', 'green'))


finally:
    status.stop()
    print('Goodbye!')

