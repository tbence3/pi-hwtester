import json
import datetime

class AbstractModel(object):
    def as_json_string(self) -> str:
        return json.dumps(self, default=lambda o: o.__dict__)

    def from_json_str(cls, json_str: str):
        json_dict = json.loads(json_str)
        return cls(**json_dict)

    def from_json(cls, json_dict: json):
        return cls(**json_dict)


class Measurement(AbstractModel):
    board = 0
    operator = 0
    status = ''
    tp5 = 0
    tp3 = 0
    tp6 = 0
    tp1 = 0
    current = 0

    def __init__(self, boardId: str, operatorId: int, circuit, message: str = None):
        self.board = boardId
        self.operator = operatorId
        self.message = message
        self.circuit = circuit
        self.date_time = str(datetime.datetime.now())

    def set_time(self, data = None):
        now = datetime.datetime.utcnow()
        self.timestamp = now.strftime("%Y-%m-%dT%H:%M:%S") + ".%03d" % (now.microsecond / 1000) + "Z"

    def set_tp5(self, data):
        self.tp5 = data
        self.tp5_unit = 'mV'

    def set_tp3(self, data):
        self.tp3 = data
        self.tp3_unit = 'mV'

    def set_tp6(self, data):
        self.tp6 = data
        self.tp6_unit = 'mV'

    def set_tp1(self, data):
        self.tp1 = data
        self.tp1_unit = 'V'

    def set_current(self, data: int):
        self.current = data
        self.current_unit = 'mA'

    def set_warning(self, message):
        self.warning = message

    def set_status(self, message):
        self.status = message


class DataClass(AbstractModel):
    boardId = 0
    operatorId = 0

    tp5 = 0
    tp3 = 0
    tp6 = 0

    dataRow = []

    def __init__(self, boardId, operatorId):
        self.boardId = boardId
        self.operatorId = operatorId

    def set_tp5(self, data):
        self.tp5 = data

    def set_tp3(self, data):
        self.tp3 = data

    def set_tp6(self, data):
        self.tp6 = data
